package com.mysmartprice.mysmartprice.ui.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mysmartprice.mysmartprice.R;
import com.mysmartprice.mysmartprice.utils.UtilsDevice;
import com.mysmartprice.mysmartprice.utils.UtilsMiscellaneous;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by arunlakra15 on 9/8/15.
 */
public abstract class NavigationViewActivity extends BaseActivity {
    @Bind(R.id.navigation_view_activity_drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.navigation_view_activity_navigation_view)
    NavigationView navigationView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.navigation_view_activity);
        ButterKnife.bind(this);

        initialize();
    }

    private void initialize() {
        setNavigationViewSize();

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,                       /* host Activity */
                mDrawerLayout,              /* DrawerLayout object */
                toolbar,                    /* Toolbar */
                R.string.drawer_open,       /* "open drawer" description */
                R.string.drawer_close       /* "close drawer" description */
        )
        {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset)
            {
                // Disables the burger/arrow animation by default
                super.onDrawerSlide(drawerView, 0);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        mDrawerToggle.syncState();
    }

    private void setNavigationViewSize() {
        int possibleMinDrawerWidth = UtilsDevice.getScreenWidth(this) -
                UtilsMiscellaneous.getThemeAttributeDimensionSize(this, R.attr.actionBarSize);
        int maxDrawerWidth = getResources().getDimensionPixelSize(R.dimen.navigation_drawer_max_width);

        navigationView.getLayoutParams().width = Math.min(possibleMinDrawerWidth, maxDrawerWidth);
    }
}
