package com.mysmartprice.mysmartprice.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mysmartprice.mysmartprice.dagger.component.ActivityComponent;

/**
 * Created by arunlakra15 on 08/07/15.
 */
public abstract class BaseActivity extends AppCompatActivity {
    ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent = ActivityComponent.Builder.initialize(this);
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }
}
