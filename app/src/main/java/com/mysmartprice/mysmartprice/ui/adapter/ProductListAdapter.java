package com.mysmartprice.mysmartprice.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


import com.mysmartprice.mysmartprice.R;
import com.mysmartprice.mysmartprice.model.ProductInfo;

import java.util.ArrayList;

/**
 * Created by mutha on 25/08/15.
 */
public class ProductListAdapter extends BaseAdapter implements AdapterView.OnItemSelectedListener{

    ArrayList <ProductInfo> productInfoList = new ArrayList<>();
    Context context;

    public ProductListAdapter(Context context ,ArrayList<ProductInfo> categoryInfoList) {
        this.productInfoList.addAll(categoryInfoList);
        this.context = context;
    }

    @Override
    public int getCount() {
        return productInfoList.size();
    }

    @Override
    public Object getItem(int position) {

        return productInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.checkbox_item, parent, false);
            holder.category = (TextView) convertView.findViewById(R.id.checkbox_text);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_box);
            //holder.subCategory = (TextView) convertView.findViewById(R.id.);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.category.setText(productInfoList.get(position).getCategory());
        holder.checkBox.setChecked(productInfoList.get(position).getChecked());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"yea",Toast.LENGTH_SHORT).show();
                CheckBox checkBox = (CheckBox)v.findViewById(R.id.checkbox_box);
               // categoryInfoList.get(position).setChecked(!checkBox.isChecked());
                checkBox.setChecked(!checkBox.isChecked());

            }
        });
        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //categoryInfoList.get(position).setChecked(!categoryInfoList.get(position).getChecked());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class ViewHolder{
        public TextView category;
        public TextView subCategory;
        public CheckBox checkBox;
    }

}
