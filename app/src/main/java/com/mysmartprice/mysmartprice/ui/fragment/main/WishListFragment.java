package com.mysmartprice.mysmartprice.ui.fragment.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mysmartprice.mysmartprice.R;
import com.
import com.mysmartprice.mysmartprice.ui.adapter.ProductListAdapter;

/**
 * Created by mutha on 05/09/15.
 */
public class WishListFragment extends Fragment{


    ProductListAdapter productListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.main,container,false);

        ListView wishList = (ListView) rootView.findViewById(R.id.product_wish_list);
        wishList.setAdapter(productListAdapter);

        return rootView;
    }
}
