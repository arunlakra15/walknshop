package com.mysmartprice.mysmartprice.ui.activity.main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.mysmartprice.mysmartprice.R;
import com.mysmartprice.mysmartprice.api.APIService;
import com.mysmartprice.mysmartprice.ui.activity.NavigationViewActivity;
import com.mysmartprice.mysmartprice.ui.fragment.main.WishListFragment;

import javax.inject.Inject;

/**
 * Created by arunlakra15 on 18/7/15.
 */
public class MainActivity extends NavigationViewActivity {
    @Inject
    APIService apiService;

    WishListFragment wishListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_container);

        wishListFragment = new WishListFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.container, wishListFragment).commit();
        getActivityComponent().inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        apiService.register("hey").subscribe();
    }
}
