package com.mysmartprice.mysmartprice.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mutha on 05/09/15.
 */
public class WishlistResponse  {


    @SerializedName("product")
    private ArrayList<ProductInfo> productInfoArrayList = new ArrayList<>();

    public ArrayList<ProductInfo> getProductInfoArrayList() {
        return productInfoArrayList;
    }

    public void setProductInfoArrayList(ArrayList<ProductInfo> productInfoArrayList) {
        this.productInfoArrayList = productInfoArrayList;
    }


}
