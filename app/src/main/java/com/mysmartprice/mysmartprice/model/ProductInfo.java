package com.mysmartprice.mysmartprice.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mutha on 05/09/15.
 */
public class ProductInfo {


    @SerializedName("category")
    private String category;

    @SerializedName("subcategory")
    private String subCategory;

    @SerializedName("category_id")
    private int id;

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
