package com.mysmartprice.mysmartprice.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by arunlakra15 on 05/09/15.
 */
public class SuccessResponse {
    @SerializedName("success")
    String success;

    public String isSuccess() {
        return success;
    }
}
