package com.mysmartprice.mysmartprice.api;

import com.mysmartprice.mysmartprice.model.SuccessResponse;

import rx.Observable;

/**
 * Created by arunlakra15 on 05/09/15.
 */
public interface APIService {
    Observable<SuccessResponse> register(String id);
}
