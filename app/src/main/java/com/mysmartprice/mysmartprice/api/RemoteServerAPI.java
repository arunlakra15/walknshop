package com.mysmartprice.mysmartprice.api;

import com.mysmartprice.mysmartprice.model.SuccessResponse;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by arunlakra15 on 08/07/15.
 * This interface will be used by Retrofit to make API calls.
 * All API calls, with their associated parameters need to be declared here.
 */
public interface RemoteServerAPI {
    String REGISTER_USER = "register.php";

    @GET("/" + REGISTER_USER)
    Observable<SuccessResponse> registerUser(@Query("id") String id);
}
