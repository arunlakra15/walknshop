package com.mysmartprice.mysmartprice.api;

import com.mysmartprice.mysmartprice.model.SuccessResponse;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by arunlakra15 on 05/09/15.
 */
public class APIServiceImpl implements APIService {
    private RemoteServerAPI remoteServerAPI;

    @Inject
    public APIServiceImpl(RemoteServerAPI remoteServerAPI) {
        this.remoteServerAPI = remoteServerAPI;
    }

    public Observable<SuccessResponse> register(String id) {
        return remoteServerAPI.registerUser(id);
    }
}
