package com.mysmartprice.mysmartprice;

import android.app.Application;
import android.content.Context;
import android.provider.SyncStateContract;
import android.util.Log;

import com.mysmartprice.mysmartprice.dagger.component.ApplicationComponent;
import com.mysmartprice.mysmartprice.utils.Constants;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.text.ParseException;


/**
 * Created by arunlakra15 on 08/07/15.
 */
public class MySmartPriceApp extends Application {
    ApplicationComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = ApplicationComponent.Builder.initialize(this);


        Parse.enableLocalDatastore(this);
        Parse.initialize(this, Constants.PARSE_APP_ID, Constants.PARSE_CLIENT_ID);


        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(com.parse.ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }

    public static ApplicationComponent getComponent(Context context) {
        return ((MySmartPriceApp) context.getApplicationContext()).appComponent;
    }
}
