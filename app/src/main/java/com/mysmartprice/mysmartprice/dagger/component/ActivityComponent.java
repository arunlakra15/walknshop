package com.mysmartprice.mysmartprice.dagger.component;

import com.mysmartprice.mysmartprice.MySmartPriceApp;
import com.mysmartprice.mysmartprice.dagger.module.ActivityScopedModule;
import com.mysmartprice.mysmartprice.dagger.scope.PerActivity;
import com.mysmartprice.mysmartprice.ui.activity.BaseActivity;
import com.mysmartprice.mysmartprice.ui.activity.NavigationViewActivity;
import com.mysmartprice.mysmartprice.ui.activity.main.MainActivity;

import dagger.Component;

/**
 * Created by arunlakra15 on 08/07/15.
 * This is an object tree which will be created only once during the life-cycle
 * of an activity.
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,  // because this component needs "Context" from ApplicationComponent
        modules = {                                 // Modules it'd use for dependency injection
                ActivityScopedModule.class,
        }
)
public interface ActivityComponent {
    // All classes which will use injections need to be declared here
    void inject(BaseActivity baseActivity);
    void inject(MainActivity mainActivity);
    void inject(NavigationViewActivity navigationViewActivity);

    // A simple Builder class to initialize component
    final class Builder {
        private Builder() { }

        public static ActivityComponent initialize(BaseActivity activity) {
            return DaggerActivityComponent.builder()
                    .applicationComponent(MySmartPriceApp.getComponent(activity))
                    .activityScopedModule(new ActivityScopedModule(activity))
                    .build();
        }
    }
}
