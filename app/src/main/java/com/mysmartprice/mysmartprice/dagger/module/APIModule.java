package com.mysmartprice.mysmartprice.dagger.module;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mysmartprice.mysmartprice.BuildConfig;
import com.mysmartprice.mysmartprice.R;
import com.mysmartprice.mysmartprice.api.APIService;
import com.mysmartprice.mysmartprice.api.APIServiceImpl;
import com.mysmartprice.mysmartprice.api.RemoteServerAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by arunlakra15 on 08/07/15.
 */
@Module
final public class APIModule {
    @Provides @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides @Singleton
    RemoteServerAPI provideRestAdapter(Context context, Gson gson) {
        return new RestAdapter.Builder()
                .setEndpoint(context.getString(R.string.api_server_address))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build()
                .create(RemoteServerAPI.class);
    }

    @Provides @Singleton
    APIService provideAPIService(APIServiceImpl apiService) {
        return apiService;
    }
}

