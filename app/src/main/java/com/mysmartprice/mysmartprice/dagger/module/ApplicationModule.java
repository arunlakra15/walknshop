package com.mysmartprice.mysmartprice.dagger.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by arunlakra15 on 08/07/15.
 */
@Module
final public class ApplicationModule {
    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides @Singleton
    Context provideApplicationContext() {
        return context;
    }
}
