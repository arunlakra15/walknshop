package com.mysmartprice.mysmartprice.dagger.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by arunlakra15 on 08/07/15.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity { }

