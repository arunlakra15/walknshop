package com.mysmartprice.mysmartprice.dagger.component;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.mysmartprice.mysmartprice.api.APIService;
import com.mysmartprice.mysmartprice.api.RemoteServerAPI;
import com.mysmartprice.mysmartprice.dagger.module.APIModule;
import com.mysmartprice.mysmartprice.dagger.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by arunlakra15 on 08/07/15.
 * This is an object tree which will be created only once during the life-cycle
 * of the application.
 * All other object trees or components will have dependency to this component.
 */
@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                APIModule.class
        }
)
public interface ApplicationComponent {
    // This filters the injections provided by its associated modules
    Context getContext();
    Gson getGson();
    RemoteServerAPI getRemoteServerAPI();
    APIService getAPIService();

    // A simple Builder class to initialize component
    final class Builder {
        public static ApplicationComponent initialize(Application app) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(app))
                    .aPIModule(new APIModule())
                    .build();
        }
    }
}
