package com.mysmartprice.mysmartprice.dagger.module;

import com.mysmartprice.mysmartprice.dagger.scope.PerActivity;
import com.mysmartprice.mysmartprice.ui.activity.BaseActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by arunlakra15 on 08/07/15.
 */
@Module
public class ActivityScopedModule {
    // All children activities, fragments, or views can use this parent activity for activity manipulations
    BaseActivity activity;

    public ActivityScopedModule(BaseActivity activity) {
        this.activity = activity;
    }

    @Provides @PerActivity
    BaseActivity provideActivity() {
        return activity;
    }
}
