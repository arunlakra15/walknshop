package com.mysmartprice.mysmartprice.utils;

import android.content.Context;
import android.content.res.TypedArray;

/**
 * Created by arunlakra15 on 8/8/15.
 */
public class UtilsMiscellaneous {
    public static int getThemeAttributeDimensionSize(Context context, int attr) {
        TypedArray a = null;
        try {
            a = context.getTheme().obtainStyledAttributes(new int[] { attr });
            return a.getDimensionPixelSize(0, 0);
        } finally {
            if(a != null) {
                a.recycle();
            }
        }
    }
}
