package com.mysmartprice.mysmartprice.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by arunlakra15 on 8/8/15.
 */
public class UtilsDevice {
    /**
     * Returns width of screen in pixels
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }
}
